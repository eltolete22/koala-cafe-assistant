from kivy.app import App
from kivy.uix.widget import Widget
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
import webbrowser


class BoxLayoutExample(BoxLayout):
    def on_button_click(self):
        webbrowser.open('https://app.hyra.io/koala/feed')
        
    def on_button_click1(self):
        webbrowser.open('https://discord.gg/ce588CzN')
        
    def on_button_click2(self):
        webbrowser.open('https://web.roblox.com/groups/937709/Koala-Association#!/about')

class MainWidget(Widget):
    pass

class KcHelperApp(App):
    pass

KcHelperApp().run()
