# Koala-Cafe-Assistant

A program to help Koala Cafe MR's/HR's with their moderation duties! 

## Getting started

First, you need to have python installed on your computer. Python is the programming language I used to make this application.

## Q&A

**Q: When I double click on the .py file, nothing happens! Why?**

A: You have to open a terminal in the folder that you are in, and type the following:
**python** (thefilename)**.py**

**Q: How can I trust this software?**

A: You can click on the python file to view its code.

**Q: What is the .kv file for?**

A: The library I used to draw a window/handle inputs was Kivy. The .kv file is basically like a configuration file for Kivy.

**Q: Why is the UI bland?**

A: 2 reasons. 1: I'm lazy. 2: I don't want to fill the program with bloat.



## CREDITS:

Kivy for their amazing and simple to understand libary!


